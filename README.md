# ansible-role-ssh-users-windows

Ansible role to configure users (with SSH keys), groups and permissions and
SSH based on roles and subjects (persons) as defined in group_vars and host_vars.

This role configures a user account and profile per role and add SSH keys for each subject.
## Requirements

Collections:
- ansible.windows 1.11.1
- community.windows 1.11.0

Install on your system with `ansible-galaxy collection install collection_name `

or add this to your repo's requirements.yml and configure ansible.cfg

```yaml
# Requirements.yml
collections:
  - name: ansible.windows
    version: 1.11.1
  - name: community.windows
    version: 1.11.
```

```ini
# ansible.cfg
[defaults]
collections_path = ./collections
```

## Example playbook

```yaml
- hosts: all
  gather_facts: true
  become: false
  tasks:
    - include_role:
        name: users
```

## Variables

### Users, groups and roles
Full example with user related variables:

Note: Add users that are admins to the **administrators** group in your config

Delete users and homedir from machines:

```yaml
user_accounts_absent:
  - fred
```

```yaml
# the role(s) to enable
user_roles_enabled:
  - operations
  - development

# the users per team and and groups

user_roles:
  - name: operations
    account: ops
    subjects:
      - caroline
      - jacob
      - rebecca
    permissions:
      groups:
        # User will be removed from any local group not defined here
        - administrators
  - name: development
    account: dev
    subjects:
      - oscar
      - james
    permissions:
      groups:
        - specialgroup
        - Remote Desktop Users

# User base per user
# shell defaults to bash
# make sure to install other type of shells otherwhise when configured here
# ssh defaults to rsa

user_subjects:
  - name: caroline
    key_type: ssh-ed25519
    public_key: AAAA

  - name: jacob
    public_key: AAAA

  - name: rebecca
    public_key: AAAA

  - name: oscar
    shell: /bin/zsh
    public_key: AAAAB

  - name: james
    public_key: AAAAB
```
